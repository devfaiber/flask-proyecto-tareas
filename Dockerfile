FROM python:3.8.5-alpine
WORKDIR /app
RUN apk add --no-cache  gcc \
                        musl-dev \
                        linux-headers \
                        python3-dev \
                        libc-dev \
                        jpeg-dev \
                        zlib-dev \
                        gpgme-dev \
                        build-base



COPY requirements.txt ./
RUN pip install -r requirements.txt

COPY ./ ./

