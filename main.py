from flask import request, make_response, redirect, render_template, url_for, session, flash
import unittest
from app import create_app
from app.firebase_service import getTodos, getTodo, putTodo, deleteTodo, storeTodo
from app.forms import AddTodo, DeleteTodoForm, ChangeStateTodoForm
from flask_login import login_required, current_user

app = create_app()
# comando para testing
@app.cli.command()
def test():
    tests = unittest.TestLoader().discover(start_dir="tests")
    unittest.TextTestRunner().run(tests)

@app.route("/")
def index():
    agent = '127.0.0.1:8000'

    response = make_response(redirect("/hello"))
    response.set_cookie('user_agent', agent)

    return response

@app.route('/hello', methods=["GET","POST"])
@login_required
def hello_world():

    agent = request.cookies.get('user_agent')
    user = current_user.id

    delete_todo_form = DeleteTodoForm()
    change_state_todo_form = ChangeStateTodoForm()
    add_todo = AddTodo()


    if add_todo.validate_on_submit():
        description = add_todo.description.data

        user_id = current_user.id

        storeTodo(user_id=user_id, description=description)
        flash("Se añadio un nuevo todo")

        return redirect(url_for("hello_world"))


    context = {
        "user_ip": agent,
        "lista": getTodos(user),
        "add_todo": add_todo,
        "change_form": change_state_todo_form,
        "delete_form": delete_todo_form,
        "username": user
    }

    template = render_template("hello.html", **context)

    return template


@app.route("/change_todo/<string:todo_id>", methods=["POST"])
def change_todo(todo_id):

    user_id = current_user.id

    todo = getTodo(user_id, todo_id)

    done = todo.to_dict()["done"]

    putTodo(user_id=user_id, todo_id=todo_id, done=done)

    flash("Se ha actualizado el estado del todo")

    return redirect(url_for("hello_world"))

@app.route("/delete_todo/<string:todo_id>", methods=["POST"])
def delete_todo(todo_id):
    user_id = current_user.id

    deleteTodo(user_id=user_id, todo_id=todo_id)

    flash("Se ha eliminado el todxo")
    return redirect(url_for("hello_world"))

@app.route("/prueba")
def prueba():
    users = getTodos("faiber")
    userId = ""
    for user in users:
        userId = user.id

    return f"Zona de { userId }"

@app.errorhandler(404)
def notFound(error):
    context = {
        "error": error
    }
    template = render_template(template_name_or_list="404.html", **context)
    return template

if __name__ == '__main__':
    app.run(debug=True)
