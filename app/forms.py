from flask_wtf import FlaskForm
from wtforms.validators import DataRequired
from wtforms.fields import StringField, SubmitField, PasswordField

class LoginForm(FlaskForm):
    username = StringField(label="Usuario: ", validators=[DataRequired("dato requerido")])
    password = PasswordField(label="Password: ", validators=[DataRequired("dato requerido")])
    botonEnviar = SubmitField(label="Login")

class SignupForm(FlaskForm):
    username = StringField(label="Usuario: ", validators=[DataRequired("datos requerido")])
    password = PasswordField(label="Password: ", validators=[DataRequired("datos requerido")])
    botonEnviar = SubmitField(label="Registrar")


class AddTodo(FlaskForm):
    description = StringField(label="Descripcion: ", validators=[DataRequired("descripcion requerida")])
    botonSave = SubmitField("guardar")

class ChangeStateTodoForm(FlaskForm):
    botonChange = SubmitField(label="Change")

class DeleteTodoForm(FlaskForm):
    botonDelete = SubmitField(label="Delete")