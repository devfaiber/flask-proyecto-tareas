from flask import render_template, flash, redirect, url_for
from flask_login import login_user, logout_user, login_required
from app.auth import auth
from app.forms import LoginForm, SignupForm
from app.models import UserData, UserModel
from app.firebase_service import getUser, putUser
from werkzeug.security import generate_password_hash, check_password_hash

@auth.route("/login", methods=["GET","POST"])
def login():
    login_form = LoginForm()
    context = {
        "loginForm": login_form
    }

    if login_form.validate_on_submit():
        usuario = login_form.username.data
        password = login_form.password.data

        user_doc = getUser(user_id=usuario)

        if user_doc.to_dict() is not None:
            password_from_db = user_doc.to_dict()["password"]
            if check_password_hash(password_from_db, password):
                user_data = UserData(username=usuario, password=password)
                user = UserModel(user_data)
                login_user(user)
                flash("Login exitoso")
                return redirect(url_for("hello_world"))
            else :
                flash("Datos incorrectos")
        else:
            flash("Datos incorrectos")

    template = render_template(template_name_or_list="login.html", **context)

    return template

@auth.route("/logout")
@login_required
def logout():
    logout_user()
    flash("regresa pronto amigo")
    return redirect(url_for("auth.login"))

@auth.route("/signup", methods=["GET", "POST"])
def signup():
    signup_form = SignupForm()
    context = {
        "signup_form": signup_form
    }

    if signup_form.validate_on_submit():
        campo_username = signup_form.username.data
        campo_password = signup_form.password.data
        password_encripted = generate_password_hash(password=campo_password)


        user_doc = getUser(campo_username)

        if not user_doc.to_dict():
            user_data = UserData(username=campo_username, password=password_encripted)

            putUser(user_data=user_data)

            flash("registrado con exito ya puedes loguearte")
            return redirect(url_for("auth.login"))
        else:
            flash("Este usuario ya esta registrado")


    template = render_template("signup.html", **context)

    return template