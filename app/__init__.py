from flask import Flask
from flask_bootstrap import Bootstrap
from flask_login import LoginManager,login_manager

from app.config import Config
from app.auth import auth
from app.models import UserModel

loginManager = LoginManager()
loginManager.login_view = "auth.login"

@loginManager.user_loader
def load_user(username):
    return UserModel.query(username)

def create_app():
    app = Flask(__name__,
                template_folder="../templates",
                static_folder="../static")
    Bootstrap(app)
    app.config.from_object(Config)
    loginManager.init_app(app,)
    app.register_blueprint(auth)

    return app