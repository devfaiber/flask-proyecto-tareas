from flask_login import UserMixin

from app.firebase_service import getUser


class UserData():
    def __init__(self, username, password):
        self.username = username
        self.password = password


class UserModel(UserMixin):
    def __init__(self, userData):
        self.id = userData.username
        self.password = userData.password

    @staticmethod
    def query(user_id):
        user_doc = getUser(user_id)
        user_data = UserData(
            username=user_doc.id,
            password=user_doc.to_dict()["password"]
        )

        return UserModel(userData=user_data)