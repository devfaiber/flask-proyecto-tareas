import firebase_admin
from firebase_admin import credentials, firestore

credentials = credentials.ApplicationDefault()

projectId = "app-tareas-287212"
if not firebase_admin._apps:
    firebase_admin.initialize_app(credentials, {
        "projectId": projectId
    })


db = firestore.client()

def getUsers():
    return db.collection("users").get()

def getUser(user_id):
    return db.collection("users")\
        .document(user_id).get()

def putUser(user_data):
    user_ref = db.collection("users").document(user_data.username)
    user_ref.set({"password": user_data.password})

def getTodos(user_id):
    return db.collection("users")\
        .document(user_id)\
        .collection("todos")\
        .get()

def getTodo(user_id, todo_id):
    return db.collection("users").document(user_id)\
            .collection("todos").document(todo_id).get()

def getTodoIdRand(user_id):
    valor = db.collection("users").document(user_id).collection("todos").get()

    todo_id = valor[0].id

    return todo_id


def storeTodo(user_id, description):
    todo_ref = db.collection("users")\
        .document(user_id)\
        .collection("todos").document()
    todo_ref.set({
        "descripcion": description,
        "done": False
    })

def putTodo(user_id, todo_id, done):
    todo_ref = db.collection("users")\
        .document(user_id)\
        .collection("todos").document(todo_id)

    done = bool(done)

    todo_ref.update({
        "done": not done
    })

def deleteTodo(user_id, todo_id):
    todo_ref = db.collection("users").document(user_id).collection("todos").document(todo_id)
    todo_ref.delete()
