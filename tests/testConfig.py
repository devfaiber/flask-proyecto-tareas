from flask_testing import TestCase
from main import app
from flask import current_app, url_for
from app.firebase_service import getTodoIdRand
import random

class TestConfig(TestCase):

    def create_app(self):
        app.config["TESTING"] = True
        app.config["WTF_CSRF_ENABLED"] = False
        app.config["LOGIN_DISABLED"] = True


        return app



    def test_exists_app(self):
        self.assertIsNotNone(current_app)

    def test_is_test_mode(self):
        self.assertTrue(current_app.config["TESTING"])

    def test_redirect_to_hello(self):
        response = self.client.get(url_for("index"))
        self.assertRedirects(response, url_for("hello_world"))

    def test_login(self):
        fake_form = {
            "username": "faiber",
            "password": "1234"
        }
        response = self.client.post(url_for("auth.login"), data=fake_form)

        self.assert_status(response, 302)
    def test_login_redirect(self):
        fake_form = {
            "username": "faiber",
            "password": "1234"
        }

        response = self.client.post(url_for("auth.login"), data=fake_form)

        self.assertRedirects(response, url_for("hello_world"))

    def test_signup_redirect(self):

        nick_rand = round(random.random()*100)

        fake_form = {
            "username": "test-" + str(nick_rand),
            "password": "1234"
        }

        response = self.client.post(url_for("auth.signup"), data=fake_form)

        self.assert_status(response, 302)
        self.assertRedirects(response, url_for("auth.login"))

    def test_template_signup(self):

        response = self.client.get(url_for("auth.signup"))

        self.assert200(response)
        self.assertTemplateUsed("signup.html")

    def test_blueprint_register(self):

        self.assertIn("auth", self.app.blueprints)

    def test_template_login_correct(self):
        self.client.get(url_for("auth.login"))
        self.assertTemplateUsed("login.html")

    def test_create_todo(self):
        fake_form = {
            "description": "test"
        }

        self.test_login()

        response = self.client.post(url_for("hello_world"), data=fake_form)

        self.assert_status(response, 302)
        self.assertRedirects(response, url_for("hello_world"))

    def test_change_todo(self):

        todo_id = getTodoIdRand("faiber")

        self.test_login()

        response = self.client.post(url_for("change_todo", todo_id=todo_id), data={})

        self.assert_status(response, 302)
        self.assertRedirects(response, url_for("hello_world"))

    def test_delete_todo(self):
        todo_id = getTodoIdRand("faiber")

        self.test_login()

        response = self.client.post(url_for("delete_todo", todo_id=todo_id), data={})

        self.assert_status(response, 302)
        self.assertRedirects(response, url_for("hello_world"))
